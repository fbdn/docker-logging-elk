# ELK Stack for Docker logs
## Usage

```bash
# Set max_map_count for ES
sudo sysctl -w vm.max_map_count=262144

# Set ELK Stack version (based on ES version)
export ELK_VERSION=7.14.1

# Build filebeat Image
docker-compose build --build-arg ELK_VERSION=$ELK_VERSION

# Pull & build containers
docker-compose up -d && docker-compose logs -f
```

See Kibana UI at http://localhost:5601
